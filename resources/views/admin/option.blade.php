@extends('admin.template')
@push('css')
	{{-- expr --}}
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Advance Options
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li class="active"><a href="#"> Options</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
         @if (Session::has('message'))
            {{-- Alert --}}
            <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
               {{ Session::get('message') }}
            </div>
            {{-- Alert --}}
         @endif
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-safari"></i></a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-facebook"></i>&nbsp<i class="fa fa-twitter"></i>&nbsp<i class="fa fa-instagram"></i></a></li>
              <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false"><i class="fa fa-whatsapp"></i></a></li>
              <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false"><i class="fa fa-book"></i></a></li>
              {{-- <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> --}}
            </ul>
            <div class="tab-content">
               <div class="tab-pane active" id="tab_1">
                  <div class="box-header with-border" style="text-align: center;">
                     <h3 class="box-title">Site Option</h3>
                  </div>
                  {!! Form::open(['url' => url('admin/option/'. $data->id), 'role' => 'form', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                  <input type="hidden" name="tab" value="1">
                  <div class="box-body">
                     <div class="form-group">
                        <label for="site_title" class="col-sm-2 control-label">Site Title</label>
                        <div class="col-sm-4">
                           <input type="text" class="form-control" id="site_title" name="site_title" placeholder="Site Title" value="{{ old('site_title', $data->site_title) }}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="site_tagline" class="col-sm-2 control-label">Site Tagline</label>
                        <div class="col-sm-4">
                           <input type="text" class="form-control" id="site_tagline" name="site_tagline" placeholder="Site Tagline" value="{{ old('site_tagline', $data->site_tagline) }}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="about" class="col-sm-2 control-label">About</label>
                        <div class="col-sm-8">
                           <textarea class="form-control" rows="15" id="about" name="about" placeholder="About" style="resize: none;" required>{{ old('about', $data->about) }}</textarea>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     
                     <button type="submit" class="btn btn-success pull-right">Submit</button>
                  </div>
                  <!-- /.box-footer -->
                  {!! Form::close() !!}
               </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <div class="box-header with-border" style="text-align: center;">
                     <h3 class="box-title">Social Media & Contact Option</h3>
                  </div>
                  {!! Form::open(['url' => url('admin/option/'. $data->id), 'role' => 'form', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                  <input type="hidden" name="tab" value="2">
                  <div class="box-body">
                     <div class="row">
                       <div class="col-md-6">
                         <div class="form-group">
                            <label for="facebook_link" class="col-sm-5 control-label"><i class="fa fa-facebook"></i></label>
                            <div class="col-sm-6">
                               <input type="text" class="form-control" id="facebook_link" name="facebook_link" placeholder="Facebook" value="{{ old('facebook_link', $data->facebook_link) }}">
                            </div>
                         </div>
                         <div class="form-group">
                            <label for="twitter_link" class="col-sm-5 control-label"><i class="fa fa-twitter"></i></label>
                            <div class="col-sm-6">
                               <input type="text" class="form-control" id="twitter_link" name="twitter_link" placeholder="Twitter" value="{{ old('twitter_link', $data->twitter_link) }}">
                            </div>
                         </div>
                         <div class="form-group">
                            <label for="instagram_link" class="col-sm-5 control-label"><i class="fa fa-instagram"></i></label>
                            <div class="col-sm-6">
                               <input type="text" class="form-control" id="instagram_link" name="instagram_link" placeholder="Site Title" value="{{ old('instagram_link', $data->instagram_link) }}">
                            </div>
                         </div>
                       </div>
                       <div class="col-md-6">
                         <div class="form-group">
                            <label for="phone" class="col-sm-1 control-label"><i class="fa fa-phone"></i></label>
                            <div class="col-sm-6">
                               <input type="text" class="form-control" id="phone" name="phone" placeholder="Site Title" value="{{ old('phone', $data->phone) }}">
                            </div>
                         </div>
                         <div class="form-group">
                            <label for="email" class="col-sm-1 control-label"><i class="fa fa-envelope"></i></label>
                            <div class="col-sm-6">
                               <input type="text" class="form-control" id="email" name="email" placeholder="Facebook" value="{{ old('email', $data->email) }}">
                            </div>
                         </div>
                         <div class="form-group">
                            <label for="address" class="col-sm-1 control-label"><i class="fa fa-map-marker"></i></label>
                            <div class="col-sm-6">
                               <input type="text" class="form-control" id="address" name="address" placeholder="Twitter" value="{{ old('address', $data->address) }}">
                            </div>
                         </div>
                       </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     
                     <button type="submit" class="btn btn-success pull-right">Submit</button>
                  </div>
                  <!-- /.box-footer -->
                  {!! Form::close() !!}
               </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
                  <div class="box-header with-border" style="text-align: center;">
                     <h3 class="box-title">Whatsapp Option</h3>
                  </div>
                  {!! Form::open(['url' => url('admin/option/'. $data->id), 'role' => 'form', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                  <input type="hidden" name="tab" value="4">
                  <div class="box-body">
                     <div class="form-group">
                        <label for="whatsapp" class="col-sm-2 control-label"><i class="fa fa-whatsapp"></i></label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="Whatsapp Number" value="{{ old('whatsapp', $data->whatsapp) }}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="whatsapp_text" class="col-sm-2 control-label">Default Text</label>
                        <div class="col-sm-8">
                           <textarea class="form-control" rows="5" id="whatsapp_text" name="whatsapp_text" placeholder="About" style="resize: none;" required>{{ old('whatsapp_text', $data->whatsapp_text) }}</textarea>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     
                     <button type="submit" class="btn btn-success pull-right">Submit</button>
                  </div>
                  <!-- /.box-footer -->
                  {!! Form::close() !!}
               </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                  <div class="box-header with-border" style="text-align: center;">
                     <h3 class="box-title">Contact and Term & Conditions</h3>
                  </div>
                  {!! Form::open(['url' => url('admin/option/'. $data->id), 'role' => 'form', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                  <input type="hidden" name="tab" value="3">
                  <div class="box-body">
                     <div class="form-group">
                        <label for="contact_text" class="col-sm-2 control-label">Contact Text</label>
                        <div class="col-sm-8">
                           <textarea class="form-control" rows="15" id="contact_text" name="contact_text" placeholder="About" style="resize: none;" required>{{ old('contact_text', $data->contact_text) }}</textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="term_condition" class="col-sm-2 control-label">Term & Conditions</label>
                        <div class="col-sm-8">
                           <textarea class="form-control" rows="15" id="term_condition" name="term_condition" placeholder="Term & Conditions" style="resize: none;" required>{{ old('term_condition', $data->term_condition) }}</textarea>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     
                     <button type="submit" class="btn btn-success pull-right">Submit</button>
                  </div>
                  <!-- /.box-footer -->
                  {!! Form::close() !!}
               </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- DataTables -->
   <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#option').addClass('active');
         $('#example1').DataTable();
      });
   </script>
@endpush