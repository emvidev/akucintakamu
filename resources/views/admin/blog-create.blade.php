@extends('admin.template')
@push('css')
	{{-- expr --}}
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Blog
         <small>Create</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li><a href="{{ url('admin/blog') }}"> Blog</a></li>
         <li class="active"><a href="#"> Create</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- right column -->
         {!! Form::open(['url' => url('admin/blog/'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
         <div class="col-md-12">
            @if($errors->any())
               <div class="alert alert-danger">
                  <ul>
                     @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
            @endif
            <!-- Horizontal Form -->
            <div class="box box-success">
               <div class="box-header with-border">
                  <h3 class="box-title">Blog Form</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <div class="box-body">
                  <div class="col-md-6" style="text-align: center; vertical-align: middle;">
                     <img id="image-preview" src="{{ asset('assets/dist/img/blank.jpg') }}"  style="max-width: 400px; max-height: 300px;" alt="">
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title') }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="short_desc" class="col-sm-2 control-label">Short Description</label>
                        <div class="col-sm-10">
                           <textarea class="form-control" rows="5" id="short_desc" name="short_desc" placeholder="Description here" style="resize: none;" required>{{ old('short_desc') }}</textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="category" name="category" placeholder="e.g. Photography" value="{{ old('category') }}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">Select Image</label>
                        <div class="col-sm-10">
                           <input type="file" id="image" name="image" onchange="previewImage();" required>
                           <p class="help-block">Example block-level help text here.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="box-header with-border">
                        <h3 class="box-title">Content</h3>
                     </div>
                     <div class="box-body">
                        <textarea class="form-control" rows="15" id="long_desc" name="long_desc" placeholder="Description here" style="resize: none;" required>{{ old('long_desc') }}</textarea>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  <a href="{{ url('admin/blog') }}" type="button" class="btn btn-default">Cancel</a>
                  <button type="submit" class="btn btn-success pull-right">Submit</button>
               </div>
               <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
          {!! Form::close() !!}
         <!-- /.row -->
      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- Bootstrap WYSIHTML5 -->
   <script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#blog').addClass('active');

         //bootstrap WYSIHTML5 - text editor
         $('#long_desc').wysihtml5()
      });

      function previewImage() {
         var reader = new FileReader();
         reader.readAsDataURL(document.getElementById("image").files[0]);

         reader.onload = function(readerEvent) {
         document.getElementById("image-preview").src = readerEvent.target.result;
         };
      }; 
   </script>
@endpush