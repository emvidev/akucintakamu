@extends('admin.template')
@push('css')
	{{-- expr --}}
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Blog
         <small>list</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li class="active"><a href="#"> Blog</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            @if (Session::has('message'))
               {{-- Alert --}}
               <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
                  {{ Session::get('message') }}
               </div>
               {{-- Alert --}}
            @endif
            <div class="box box-success">
               <!-- /.box-header -->
               <div class="box-header">
                  <a href="{{ url('admin/blog/create') }}" class="btn btn-success" style="width: 50px; height: 25px;"><i class="fa fa-plus"></i></a>
               </div>
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th style="width: 30px; text-align: center;">Date (d/m/y)</th>
                           <th>Title</th>
                           <th>Category</th>
                           <th style="width: 10%;">#</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($data as $d)
                           <tr>
                              <td style="text-align: center;">{{ \Carbon\Carbon::parse($d->created_at)->format('d/m/y') }}</td>
                              <td>{{ $d->title }}</td>
                              <td>{{ $d->category }}</td>
                              <td align="center">
                                 <div class="btn-group">
                                    {!! Form::open(['url' => url('admin/blog/'. $d->id), 'role' => 'form', 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                                       <a href="{{ url('admin/blog/'. $d->id .'/edit') }}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                       <button type="submit" name="submit" class="btn btn-danger" onclick="return confirm('Delete data..?');"><i class="fa fa-trash"></i></button>
                                    {!! Form::close() !!}
                                 </div>
                              </td>
                           </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- DataTables -->
   <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#blog').addClass('active');
         $('#example1').DataTable({
            'sorting' : false
         });
      });
   </script>
@endpush