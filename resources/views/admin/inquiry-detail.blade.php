@extends('admin.template')
@push('css')
	{{-- expr --}}
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Inquiry
         <small>detail</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li><a href="{{ url('admin/inquiry') }}"> Inquiry</a></li>
         <li class="active"><a href="#"> Detail</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3>{{ $data->subject }}</h3>
                <h5>From: {{ $data->email }}
                  <span class="mailbox-read-time pull-right">{{ \Carbon\Carbon::parse($data->created_at)->format('d M Y'. ' - ' .'H:i A') }}</span></h5>
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <p>{{ $data->message }}</p>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
            <div class="box-footer">
              <div class="pull-right">
               {!! Form::open(['url' => url('admin/inquiry/' . $data->id), 'role' => 'form', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                  <a type="button" class="btn btn-default" href="mailto:{{ $data->email }}?subject=RE:{{ $data->subject }}&body=reply%20for%20message%20:%20{{ $data->message }}"><i class="fa fa-reply"></i> Reply</a>
                  <button type="submit" class="btn btn-default" onclick="return confirm('Mark as unread?');"><i class="fa fa-envelope"></i> Mark as unread</button>
               {!! Form::close() !!}
              </div>
              {!! Form::open(['url' => url('admin/inquiry/'. $data->id), 'role' => 'form', 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                 <button type="submit" class="btn btn-danger" onclick="return confirm('Delete..?');"><i class="fa fa-trash-o"></i> Delete</button>
                 {{-- <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button> --}}
              {!! Form::close() !!}
            </div>
            <!-- /.box-footer -->
            </div>
             <!-- /. box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- DataTables -->
   <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#inquiry').addClass('active');
         $('#example1').DataTable();
      });
   </script>
@endpush