@extends('admin.template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         User
         <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li><a href="{{ url('admin/user') }}"> User</a></li>
         <li class="active"><a href="#"> Edit</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- right column -->
         <div class="col-md-6">
            @if($errors->any())
               <div class="alert alert-danger">
                  <ul>
                     @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
            @endif
            @if (Session::has('message'))
               {{-- Alert --}}
               <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Opss..!</h4>
                  {{ Session::get('message') }}
               </div>
               {{-- Alert --}}
            @endif
            <!-- Horizontal Form -->
            <div class="box box-success">
               <div class="box-header with-border">
                  <h3 class="box-title">User Form</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               {!! Form::open(['url' => url('admin/user/' . $data->id), 'role' => 'form', 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                  <div class="box-body">
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name', $data->name) }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                           <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email', $data->email) }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                           <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="password_confirmation" class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-10">
                           <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                        </div>
                     </div>
                     <div class="form-group">
                         <label for="role" class="col-sm-2 control-label">Promo</label>
                         <div class="col-sm-5">
                           <select class="form-control" id="role" name="role">
                              <option value="super user" {{ $data->role == 'super admin' ? 'selected' : '' }}>Super User</option>
                              <option value="admin" {{ $data->role == 'admin' ? 'selected' : '' }}>Admin</option>
                              <option value="reservation" {{ $data->role == 'reservation' ? 'selected' : '' }}>Reservation</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     <a href="{{ url('admin/user') }}" type="button" class="btn btn-default">Cancel</a>
                     <button type="submit" class="btn btn-success pull-right">Submit</button>
                  </div>
                  <!-- /.box-footer -->
               {!! Form::close() !!}
            </div>
            <!-- /.box -->
         </div>
         <!-- /.row -->
      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#user').addClass('active');
      });
   </script>
@endpush