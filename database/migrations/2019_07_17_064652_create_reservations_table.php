<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id');
            $table->integer('guest_id')->nullable();

            $table->string('code', 10);
            $table->date('date');
            $table->decimal('price', 20, 2);
            $table->enum('status', ['BOOKED', 'CONFIRMED', 'COMPLETED', 'PAID'])->nullable()->default('BOOKED');
            $table->string('payment_proof', 75)->nullable();
            $table->string('chat_proof', 500)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
