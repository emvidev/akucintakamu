<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Testimonial;

use Illuminate\Validation\Rule;
use Session;
use Validator;
use Image;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Testimonial::orderByDesc('created_at')->get();

        return view('admin.testimonial-index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                'max:75'
            ],
            'from' => 'required|max:50',
            'comment' => 'required|max:250'
        ]);
        if ($validator->fails()) {
            Session::flash('error');

            return redirect('admin/testimonial')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Testimonial;

        $data->name = ucwords($request->name);
        $data->from = ucwords($request->from);
        $data->comment = $request->comment;

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Data has stored..');

        return redirect('admin/testimonial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Testimonial::find($id)->delete();

        Session::flash('type', 'danger');
        Session::flash('icon', 'times');
        Session::flash('message', 'Data has deleted..');

        return redirect('admin/testimonial');
    }
}
