<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\User;
use Auth;
use Validator;

class LoginController extends Controller
{
    //
    public function index()
    {
    	return view('login');
    }

    public function doLogin(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'email' => 'email|required',
    		'password' => 'required'
    	]);
    	if ($validator->fails()) {
    		return redirect('login')
    			->withErrors($validator)
    			->withInput();
    	}

    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    		return redirect()->intended('/admin');
    	}else{
    		return redirect('login')->withErrors([
    			'error' => true
    		]);
    	}
    }

    public function doLogout()
    {
    	Auth::logout();

    	return redirect('login');
    }
}
