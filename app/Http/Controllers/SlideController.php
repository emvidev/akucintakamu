<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slide;

use Illuminate\Validation\Rule;
use Session;
use Validator;
use Image;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Slide::orderByDesc('page')->paginate('12');

        return view('admin.slide-index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                'max:50'
            ],
            'content' => 'required|max:250',
            'image' => 'required|image',
            'page' => 'required'
        ]);
        if ($validator->fails()) {
            Session::flash('error');

            return redirect('admin/slide')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Slide;

        $data->title = ucwords($request->title);
        $data->content = $request->content;
        $data->link = $request->link;
        $data->page = $request->page;

        if ($request->hasFile('image')) {
            $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/slide/';

            $data->image = $img;

            Image::make($request->image)->save($base_path.$img);
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'New slide has created..');

        return redirect('admin/slide');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Slide::find($id)->delete();

        Session::flash('type', 'danger');
        Session::flash('icon', 'times');
        Session::flash('message', 'Slide image was deleted..');

        return redirect('admin/slide');
    }
}
