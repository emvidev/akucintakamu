<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Guest;
use App\Reservation;
use App\Gallery;
use App\Product;
use App\Slide;
use App\Testimonial;
use App\Option;
use App\Post;
use App\Inquiry;

use Illuminate\Validation\Rule;
use Session;
use Validator;
use DB;
use Carbon\Carbon;

class FoController extends Controller
{
    //
   	public function getHome(Request $request)
   	{
       if (isset($request->c)) {
          $gallery = Gallery::where('category', $request->c)->get();
       }else{
          $gallery = Gallery::all();
       }

   		$slide = Slide::where('page', 'HOME')->get();
      $testimonial = Testimonial::orderBy('created_at')->limit(3)->get();

   		return view('home', [
   			'slide' => $slide,
            'testimonial' => $testimonial,
            'gallery' => $gallery
   		]);
   	}

   	public function getReservation(Request $request)
   	{
         $p = $request->product;
         $d = Carbon::parse($request->date)->format('Y-m-d');
   		$slide = Slide::where('page', 'RESERVATION')->get();
         $product = Product::All();
         $reservation = Reservation::where('product_id', $p)->where('date', $d)->get();


            // $validator = Validator::make($request->all(), [
            //       'product' => 'required',
            //       'date' => 'required|after:today'
            //   ]);
            //   if ($validator->fails()) {
            //       Session::flash('type', 'error');
            //       Session::flash('message', 'Product not available on selected date');

            //       return redirect('reservation');
            //   }

         if ($request->q) {
            $validator = Validator::make($request->all(), [
               'product' => 'required',
               'date' => 'required|after:today'
            ]);
            if ($validator->fails()) {
               Session::flash('type', 'error');
               Session::flash('message', 'Please choose date after today');

               return redirect('reservation');
            }

            if (count($reservation) ==  1) {
               Session::flash('type', 'error');
               Session::flash('message', 'Product not available on selected date');

               return redirect('reservation');
            }else{
               $product_result = Product::find($p);

               return view('reservation', [
                  'slide' => $slide,
                  'product' => $product,
                  'product_result' => $product_result,
                  'q' => 1
               ]);
            }
         }else{
            return view('reservation', [
               'slide' => $slide,
               'product' => $product
            ]);
         }
   	}

      public function getReservationDetail(Request $request, $product)
      {
         $slide = Slide::where('page', 'RESERVATION')->get();
         $product = Product::where('name', $product)->first();
         $selected_date = $request->date;

         return view('reservation-detail', [
            'slide' => $slide,
            'product' => $product,
            'selected_date' => $selected_date
         ]);
      }

      public function newReservation(Request $request, $product)
      {
         $slide = Slide::where('page', 'RESERVATION')->get();
         $productx = Product::find($product);
         $selected_date = $request->date;
         $guest_name = $request->contact_name;
         $guest_email = $request->contact_email;
         $guest_phone = $request->contact_phone;

         $guest_checked = Guest::where('name', $guest_name)
               ->where('email', $guest_email)
               ->first();

         // return $productx->price;

         if ($guest_checked) {
            $rsv = new Reservation;

            $rsv->product()->associate($productx->id);
            $rsv->guest()->associate($guest_checked->id);
            $rsv->code = str_random(7);
            $rsv->date = Carbon::parse($selected_date)->format('Y-m-d');
            $rsv->price = $productx->price;
            $rsv->status = 'BOOKED';

            $rsv->save();

            Session::flash('guest_name', $guest_name);
            Session::flash('guest_email', $guest_email);
            Session::flash('guest_phone', $guest_phone);
            Session::flash('message', 'Booking has made successfully');

            return redirect('reservation/detail/'. $productx->name. '?date='. $selected_date);
         }else{
            $guest = new Guest;

            $guest->name = ucwords($guest_name);
            $guest->email = $guest_email;
            $guest->phone = $guest_phone;

            $guest->save();

            $rsv = new Reservation;

            $rsv->product()->associate($productx->id);
            $rsv->guest()->associate($guest->id);
            $rsv->code = str_random(7);
            $rsv->date = Carbon::parse($selected_date)->format('Y-m-d');
            $rsv->price = $productx->price;
            $rsv->status = 'BOOKED';

            $rsv->save();

            Session::flash('guest_name', $guest_name);
            Session::flash('guest_email', $guest_email);
            Session::flash('guest_phone', $guest_phone);
            Session::flash('message', 'Booking has made successfully');

            return redirect('reservation/detail/'. $productx->name. '?date='. $selected_date);
         }

         // return view('reservation-detail', [
         //    'slide' => $slide,
         //    'product' => $product,
         //    'selected_date' => $selected_date
         // ]);
      }

   	public function getGallery(Request $request)
   	{
      $gallery = Gallery::All();
      // return $gallery;
   		return view('gallery', [
   			'gallery' => $gallery
   		]);
   	}

      public function getGalleryDetail($title)
      {
         $gallery = Gallery::where('name', $title)->first();
         $slide = Slide::where('page', 'GALLERY')->get();

         return view('gallery-detail', [
            'gallery' => $gallery,
            'slide' => $slide
         ]);
      }

   	public function getPricePromo(Request $request)
   	{
         $slide = Slide::where('page', 'PRICE & PROMO')->get();
   		if (isset($request->c)) {
            $product = Product::where('category', $request->c)->paginate('5');
         }else{
            $product = Product::paginate('5');
         }

   		return view('price-promo', [
   			'product' => $product,
            'slide' => $slide
   		]);
   	}

      public function getProduct($name)
      {
         $product = Product::where('name', $name)->first();

         return view('product', [
            'product' =>$product
         ]);
      }

   	public function getContact()
   	{
   		$option = Option::find(1);;

   		return view('contact-us', [
   			'option' => $option
   		]);
   	}

      public function sendMessage(Request $request)
      {
         $validator = Validator::make($request->all(), [
            'contact_name' => 'required|max:75',
            'contact_email' => 'required|max:50',
            'contact_subject' => 'required|max:30',
            'contact_message' => 'required|max:500',
         ]);

         $data = new Inquiry;

         $data->name = ucwords($request->contact_name);
         $data->email = $request->contact_email;
         $data->subject = ucwords($request->contact_subject);
         $data->message = $request->contact_message;

         $data->save();

         Session::flash('type', 'success');
         Session::flash('icon', 'check');
         Session::flash('message', 'Message sent..');

         return redirect('contact-us');
      }

   	public function getTermCondition()
   	{
   		$option = Option::find(1);

   		return view('term-condition', [
   			'option' => $option
   		]);
   	}

   	public function getPost()
   	{
   		$post = Post::paginate(9);

   		return view('blog', [
   			'post' => $post
   		]);
   	}

   	public function getPostDetail($title)
   	{
   		$post = Post::where('title', $title)->first();

   		return view('blog-detail', [
   			'post' => $post
   		]);
   	}
}
